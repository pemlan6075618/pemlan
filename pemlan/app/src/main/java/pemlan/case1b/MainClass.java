package pemlan.case1b;

import pemlan.case1a.*;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainClass {

    public static void main(String[] a) {
        ArrayList barangs=new ArrayList();

        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 4; i++) {
            System.out.print("masukkan nama barang: ");
            String nama = scanner.nextLine();

            System.out.print("masukkan harga barang: ");
            Integer harga = scanner.nextInt();

            System.out.print("masukkan jumlah barang: ");
            Integer jumlah = scanner.nextInt();

            System.out.print("masukkan jumlah stok barang: ");
            Integer jmlStok = scanner.nextInt();

            Barang b = new Barang();
            b.setNama(nama);
            b.setHarga(harga);
            b.setJumlah(jumlah); 
            b.setJmlStokToko(jmlStok);
            barangs.add(b);
            scanner.nextLine();
        }
        System.out.println("==================================");
        Integer total = 0;
        for (int i = 0; i < 4; i++) {
            Barang b =(Barang)barangs.get(i);
            System.out.println(b.getNama() + " : " + b.getHarga() + " : " + b.getJumlah()+" : "+b.getJumlah()*b.getHarga());
            total = total + (b.getHarga() * b.getJumlah());
        }
        System.out.println("Total Bayar " + total);
    }
}