package pemlan.case1b;

public class Barang {

    private String nama;
    private Integer harga;
    private Integer jumlah;
    private Integer jmlStokToko;

    public Integer getJmlStokToko() {
        return jmlStokToko;
    }

    public void setJmlStokToko(Integer jmlStokToko) {
        this.jmlStokToko = jmlStokToko;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Integer getHarga() {
        return harga;
    }

    public void setHarga(Integer harga) {
        this.harga = harga;
    }

    public Integer getJumlah() {
        return jumlah;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }
}
