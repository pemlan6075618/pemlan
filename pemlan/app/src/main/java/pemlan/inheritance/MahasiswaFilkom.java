package pemlan.inheritance;

public class MahasiswaFilkom extends Mahasiswa {

    public void aksesFilkomApps() {
        System.out.println("Mahasiswa " + this.nama 
                + " mengakses filkomapps");
    }
}
