package pemlan.generics;

import java.util.ArrayList;

public class MainClass {

    public static void main(String[] a) {
        //non generic
        ArrayList l1 = new ArrayList();
        l1.add("Non Generic 1");
        String item1 = (String) l1.get(0);

//        Object o=l1.get(0);
//        String item11=(String)o;
        l1.add(1);
        Integer item2 = (Integer) l1.get(1);

        Integer k = 0;
        for (int i = 0; i < l1.size(); i++) {
            k = k + (Integer) l1.get(i);
        }

        //generic
        ArrayList<String> l2 = new ArrayList<String>();
        l2.add("Generic 1");
        String item3 = l2.get(0);
//        l2.add(1);

        ArrayList<Integer> l3 = new ArrayList<Integer>();
        l3.add(5);
        l3.add(8);
        l3.add(4);
        Integer j = 0;
        for (int i = 0; i < l3.size(); i++) {
            j = j + l3.get(i);
        }

        AllInOne<String, Integer> s1 = new AllInOne<String, Integer>();
        s1.setVar1("var 1 string");
        s1.setVar2(1);
        String r1 = s1.getVar1();

        AllInOne<Integer, Integer> i1 = new AllInOne<Integer, Integer>();
        i1.setVar1(1);
        i1.setVar2(5);
        Integer r2 = i1.getVar1();
        
    }
}
