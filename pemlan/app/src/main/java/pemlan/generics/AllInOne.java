package pemlan.generics;

public class AllInOne<K,L> {
    private K var1;
    private L var2;

    public K getVar1() {
        return var1;
    }

    public void setVar1(K var1) {
        this.var1 = var1;
    }

    public L getVar2() {
        return var2;
    }

    public void setVar2(L var2) {
        this.var2 = var2;
    }
        
}
