package pemlan.konstruktor;

public class Mobil {

    public String warna;
    public String pabrikan;
    public String jenis;

    public Mobil(String warna) {
        this.warna = warna;
        System.out.println("konstruktor mobil "+ warna);
    }

    public Mobil(Integer kecepatan) {
        System.out.println("konstruktor mobil berjalan dengan kecepatan "+kecepatan);
    }

    public Mobil(String warna, String jenis) {
        this.warna = warna;
        this.jenis = jenis;
        System.out.println("konstruktor mobil "+jenis+" berwearna "+warna);
    }

    public void berjalan() {
        System.out.println("mobil " + pabrikan + " " + jenis + " " 
                + warna + " berjalan 1 km");
    }

    public void berjalan(Integer jarak) {
        System.out.println("mobil " + pabrikan + " " + jenis + " " + warna + " berjalan "+jarak);
    }

    public void berjalan(String warna, Integer jarak) {
        System.out.println("mobil " + pabrikan + " " + jenis + " " + warna + " berjalan "+jarak);
    }

    public void berjalan(Integer jarak, String warna) {
        System.out.println("mobil " + pabrikan + " " + jenis + " " + warna + " berjalan "+jarak);
    }
    
    public void berhenti() {
        System.out.println("mobil " + warna + " berhenti");
    }
}
