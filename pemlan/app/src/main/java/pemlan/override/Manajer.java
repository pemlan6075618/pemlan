package pemlan.override;

public class Manajer extends Karyawan {

    public void rekapLaporan() {
        System.out.println("Pekerjaan maanger adalah membuat laporan bulanan");
    }

    public void hitungGaji() {
        System.out.println("Manajer gajinya Rp. 10.000.000");
    }

}
