package pemlan.abstractinterface;

public class Sapi extends Binatang implements Mamalia {
    
    public void makan() {
        System.out.println("Sapi makan rumput");
    }
    
    public void menyusui() {
        System.out.println("Sapi menyusui");
    }
}
