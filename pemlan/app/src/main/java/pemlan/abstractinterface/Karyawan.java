package pemlan.abstractinterface;

public abstract class Karyawan {

    public String id;
    public String nama;

    public abstract void hitungGaji();
    
    public void details()
    {
        System.out.println("Nama Karyawan :"+nama);
    }
}