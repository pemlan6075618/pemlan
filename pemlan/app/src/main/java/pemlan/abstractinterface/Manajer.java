package pemlan.abstractinterface;

public class Manajer extends Karyawan {

    public Integer upahHarian = 100;

    public void rekapLaporan() {
        System.out.println("Pekerjaan maanger adalah membuat laporan bulanan");
    }

    @Override
    public void hitungGaji() {
        System.out.println("Manajer gajinya Rp 5000000");
    }
}
