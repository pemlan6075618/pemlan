package pemlan.abstractinterface;

public class Serigala extends Binatang implements Predator, Mamalia {

    @Override
    public void makan() {
        System.out.println("Serigala makan daging");
    }

    @Override
    public void berburu() {
        System.out.println("Serigala berburu");
    }

    @Override
    public void berlari() {
        System.out.println("Serigala istirahat");
    }

    @Override
    public void menyusui() {
        System.out.println("Serigala manyusui");
    }
}
