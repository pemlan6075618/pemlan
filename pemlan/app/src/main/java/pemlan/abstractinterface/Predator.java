package pemlan.abstractinterface;

public interface Predator {

    void berburu();

    void berlari();
}
