package pemlan.abstractinterface;

public class Harimau extends Binatang implements Predator, Mamalia {

    public void makan() {
        System.out.println("Harimau makan daging");
    }

    @Override
    public void berburu() {
        System.out.println("Harimau berburu");
    }

    @Override
    public void berlari() {
        System.out.println("Harimau berlari");
    }

    @Override
    public void menyusui() {
        System.out.println("Harimau menyusui");
    }
}
