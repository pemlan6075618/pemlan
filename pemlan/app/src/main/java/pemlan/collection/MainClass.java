package pemlan.collection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

public class MainClass {

    public static void main(String[] a) {
        System.out.println("* ArrayList");
        ArrayList<String> l1 = new ArrayList<String>();
        l1.add("satu");
        l1.add("dua");
        l1.add("tiga");
        System.out.println("* ArrayList cara 1");
        for(int i=0;i<l1.size();i++)
        {
            System.out.println(l1.get(i));
        }
        System.out.println("* ArrayList cara 2");        
        for (String item : l1) {
            System.out.println(item);
        }
        
        ArrayList<Mobil> l2=new ArrayList<Mobil>();
        Mobil mbl1=new Mobil();
        mbl1.setJenis("SUV");
        mbl1.setWarna("PUTIH");
        l2.add(mbl1);

        System.out.println("* Vector");
        Vector<String> v1 = new Vector<String>();
        v1.add("satu");
        v1.add("dua");
        v1.add("tiga");
        for (String item : v1) {
            System.out.println(item);
        }

        System.out.println("* HashSet");
        Set<Integer> s1 = new HashSet<Integer>();
        s1.add(1);
        s1.add(2);
        s1.add(3);
        s1.add(3);
        for (Integer i : s1) {
            System.out.println(i);
        }

        Map<String, Integer> m1 = new HashMap<String, Integer>();
        m1.put("A", 2);
        m1.put("B", 2);
        m1.put("B", 3);
        Integer i=m1.get("B");
//cara 1
        System.out.println("* Map cara 1");
        Set<String> s = m1.keySet();
        for (String key : s) {
            System.out.println(key + " : " + m1.get(key));
        }
//cara 2
        System.out.println("* Map cara 2");
        for (Map.Entry<String, Integer> e : m1.entrySet()) {
            System.out.println(e.getKey() + " : " + e.getValue());
        }
    }
}
