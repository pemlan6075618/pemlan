package pemlan.exception;

import java.io.File;
import java.io.IOException;

public class MainClass5 {

    public static void main(String[] a) {
        Kalkulator k = new Kalkulator();
        Integer hasilBagi = k.bagi(4, 0); // terjadi aritmethicexception
        System.out.println("Hasil pembagian : " + hasilBagi);

        Kalkulator k2 = null;
        Integer hasil2 = k2.kali(3, 4);

        File file = new File("C://nama.txt");

        try {
            file.createNewFile();
        } catch (IOException e) {
            System.out.println("checkedException");
        }
        System.out.println(" 3 SELESAI");
    }
}
