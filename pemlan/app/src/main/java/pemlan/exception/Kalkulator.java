package pemlan.exception;

/**
 *
 * @author djoko
 */
public class Kalkulator {

    public Integer kali(Integer a, Integer b) {
        System.out.println(" 1 Operasi perkalian");
        return a * b;
    }

    public Integer bagi(Integer a, Integer b) {
        System.out.println("1 Operasi pembagian");
        return a / b;
    }

    public Integer bagi2(Integer a, Integer b) {
        try {
            System.out.println("1 Operasi pembagian");
            return a / b;
        } catch (ArithmeticException e) {
            return 0;
        }
    }

    public Integer bagi3(Integer a, Integer b) {
        System.out.println("1 Operasi pembagian");
        if (b == 0) {
            throw new MyException();
        } else {
            return a / b;
        }
    }

    public Integer bagi4(Integer a, Integer b) throws MyException2 {
        System.out.println("1 Operasi pembagian");
        if (b == 0) {
            throw new MyException2();
        } else {
            return a / b;
        }
    }

}
