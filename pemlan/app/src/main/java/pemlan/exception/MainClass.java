package pemlan.exception;

public class MainClass {

    public static void main(String[] a) {
        Kalkulator k = new Kalkulator();
        Integer hasil = k.kali(2, 2);
        System.out.println("2 Hasil perkalian : " + hasil);

        try {
            Integer hasilBagi = k.bagi(4, 2);
            System.out.println("Hasil pembagian : " + hasilBagi);

            Kalkulator k2 = null;
            k2.kali(2, 2);
        } catch (ArithmeticException d) {
            System.out.println(" 1 Terjadi exception " + d.getMessage());
        } catch (NullPointerException ne) {
            System.out.println(" 2 Terjadi exception " + ne.getMessage());
        } catch (Exception e) {
            System.out.println(" 3 Terjadi exception " + e.getMessage());
        }
        System.out.println(" 3 SELESAI");
    }
}
