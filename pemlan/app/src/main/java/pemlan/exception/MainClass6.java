package pemlan.exception;

import java.io.File;
import java.io.IOException;

public class MainClass6 {

    public static void main(String[] a) {
        MainClass6 c = new MainClass6();
//        c.test1();
//        c.test2();
//        c.test3();
        try {
            c.test4();
        } catch (MyException2 e) {
            System.out.println(e.getMessage());
        }
        System.out.println(" 3 SELESAI");
    }

    public void test1() {
        Kalkulator k = new Kalkulator();
        Integer hasilBagi = k.bagi2(4, 0); // terjadi aritmethicexception
        System.out.println("Hasil pembagian : " + hasilBagi);
    }

    public void test2() {
        Kalkulator k = new Kalkulator();
        try {
            Integer hasilBagi = k.bagi3(4, 0); // terjadi aritmethicexception
            System.out.println("Hasil pembagian : " + hasilBagi);
        } catch (MyException e) {
            System.out.println(e.getMessage());
        }
    }

    public void test3() {
        Kalkulator k = new Kalkulator();
        try {
            Integer hasilBagi = k.bagi4(4, 0); // terjadi aritmethicexception
            System.out.println("Hasil pembagian : " + hasilBagi);
        } catch (MyException2 e) {
            System.out.println(e.getMessage());
        }
    }

    public void test4() throws MyException2 {
        Kalkulator k = new Kalkulator();
        Integer hasilBagi = k.bagi4(4, 0); // terjadi MyException2
        System.out.println("Hasil pembagian : " + hasilBagi);
    }
}
