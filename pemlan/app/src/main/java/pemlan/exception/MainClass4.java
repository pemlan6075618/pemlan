package pemlan.exception;

public class MainClass4 {

    public static void main(String[] a) {
        Kalkulator k = new Kalkulator();
        try {
            Integer hasilBagi = k.bagi(4, 0); // terjadi aritmethicexception
            System.out.println("Hasil pembagian : " + hasilBagi);

            Kalkulator k2 = null;
            Integer hasil2 = k2.kali(3, 4);
        } catch (ArithmeticException d) {
            System.out.println(" 1 Terjadi exception " + d.getMessage());
//            d.printStackTrace();
        } finally {
            System.out.println(" 2 Statement FINALLY");
        }
        System.out.println(" 3 SELESAI");
    }
}
