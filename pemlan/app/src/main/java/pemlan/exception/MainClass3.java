package pemlan.exception;

public class MainClass3 {

    public static void main(String[] a) {
        Kalkulator k = new Kalkulator();
        try {
            Integer hasilBagi = k.bagi(4, 0); // terjadi aritmethicexception
            System.out.println("Hasil pembagian : " + hasilBagi);

            Kalkulator k2 = null;
            Integer hasil2 = k2.kali(3, 4);
        } catch (NullPointerException d) {
            System.out.println(" 1 Terjadi exception " + d.getMessage());
        } finally {
            System.out.println(" 2 Statement FINALLY");
        }
        System.out.println(" 3 SELESAI");
    }
}
