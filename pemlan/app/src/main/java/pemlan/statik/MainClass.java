package pemlan.statik;

public class MainClass {

    public static void main(String[] a) {
        Mobil mobil1 = new Mobil();
        mobil1.warna = "MERAH";
        mobil1.diProduksi();

        System.out.println("mobil1 warna " + mobil1.warna);
        System.out.println("jumlah mobil yang diproduksi "
                + Mobil.jmlProduksi);

        Mobil mobil2 = new Mobil();
        mobil2.warna = "PUTIH";
        mobil2.diProduksi();
        System.out.println("mobil2 warna " + mobil2.warna);
        System.out.println("jumlah mobil2 yang diproduksi " + mobil2.jmlProduksi);
        System.out.println("jumlah mobil1 yang diproduksi " + mobil1.jmlProduksi);
        System.out.println("jumlah mobil1 yang diproduksi " + Mobil.jmlProduksi);

        Mobil.diProduksi();
        Mobil.jmlProduksi=100;
        System.out.println("jumlah mobil1 yang diproduksi " + mobil1.jmlProduksi);

    }
}
