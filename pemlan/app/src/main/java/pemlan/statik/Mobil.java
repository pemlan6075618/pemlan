package pemlan.statik;

public class Mobil {

    public String warna;
    public String pabrikan;
    public String jenis;
    public static int jmlProduksi;
    public static final int jmlRoda=4;

    public static void diProduksi()
    {
        jmlProduksi=jmlProduksi+1;
        
    }
    
    public void berjalan(Integer jarak) {
        jmlProduksi=jmlProduksi+1;
        System.out.println("mobil " + pabrikan + " " + jenis + " " + warna + " berjalan "+jarak);
    }

    public void berhenti() {
        System.out.println("mobil " + warna + " berhenti");
    }
    
}
