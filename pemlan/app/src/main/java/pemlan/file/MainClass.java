package pemlan.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainClass {

    public static void main(String[] a) {
        File f1 = new File("D://data.txt");
        if (f1.exists()) {
            System.out.println("File sudah ada");
            try {
                BufferedReader in = new BufferedReader(new FileReader(f1));
                String s = in.readLine();
                while (s != null) {
                    System.out.println(s);
                    s = in.readLine();
                }
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else {
            System.out.println("File belum ada");
            FileOutputStream fout = null;
            try {
                fout = new FileOutputStream(f1);
                String s = "Hello World";
                fout.write(s.getBytes());
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            } catch (IOException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    fout.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
