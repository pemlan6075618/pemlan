package pemlan.case3b;

import java.util.ArrayList;
import java.util.Scanner;

public class MainClass {

    public static void main(String[] a) {
        ArrayList mahasiswas = new ArrayList();

        Scanner scanner = new Scanner(System.in);
        boolean next = true;
        while (next) {
            System.out.print("masukkan nim : ");
            String nim = scanner.nextLine();

            System.out.print("masukkan nama : ");
            String nama = scanner.nextLine();

            System.out.print("masukkan alamat: ");
            String alamat = scanner.nextLine();

            Mahasiswa mhs = new Mahasiswa();
            mhs.setNim(nim);
            mhs.setNama(nama);
            mhs.setAlamat(alamat);

            ArrayList<Mobil> mobils = new ArrayList();
            boolean nextMobil = true;
            while (nextMobil) {
                System.out.print("masukkan warna mobil : ");
                String warna = scanner.nextLine();

                System.out.print("masukkan jenis mobil : ");
                String jenis = scanner.nextLine();

                System.out.print("masukkan pabrikan mobil : ");
                String pabrikan = scanner.nextLine();

                Mobil mobil = new Mobil();
                mobil.setWarna(warna);
                mobil.setJenis(jenis);
                mobil.setPabrikan(pabrikan);
                mobils.add(mobil);
                System.out.print("tambah mobil? ");
                String tambahMobil = scanner.nextLine();

                if (tambahMobil.equals("t")) {
                    nextMobil = false;
                }
            }

            mhs.setMobils(mobils);
            mahasiswas.add(mhs);
            System.out.print("tambah lagi? ");
            String tambah = scanner.nextLine();

            if (tambah.equals("t")) {
                next = false;
            }
        }
        System.out.println("==================================");
        for (int i = 0; i < mahasiswas.size(); i++) {
            Mahasiswa mhs = (Mahasiswa) mahasiswas.get(i);
            System.out.println(mhs.getNim() + " | " + mhs.getNama()
                    + " | " + mhs.getAlamat());
            ArrayList mobils = mhs.getMobils();
            for (int j = 0; j < mobils.size(); j++) {
                Mobil mbl = (Mobil) mobils.get(j);
                System.out.println(" - |"+mbl.getWarna() + " | " + mbl.getJenis()
                        + " | " + mbl.getPabrikan());
            }
        }
    }
}
