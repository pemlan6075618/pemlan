package pemlan.case3b;

import java.util.ArrayList;

public class Mahasiswa {

    private String nim;
    private String nama;
    private String alamat;
    private ArrayList<Mobil> mobils=new ArrayList();

    public ArrayList<Mobil> getMobils() {
        return mobils;
    }

    public void setMobils(ArrayList<Mobil> mobils) {
        this.mobils = mobils;
    }

    
    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

}
