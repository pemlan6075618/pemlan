package pemlan.polymorphism;

import pemlan.override.*;

public class MainClass {

    public static void main(String[] a) {
        Karyawan mnj1 = new Manajer();
//        mnj1.hitungGaji();        
        Karyawan eng1 = new Engineer();
//        eng1.hitungGaji();
        bayarGaji(mnj1);
        bayarGaji(eng1);
        kerja(mnj1);
        kerja(eng1);
    }

    public static void bayarGaji(Karyawan k) {
        System.out.println("membayar gaji karyawan :");
        k.hitungGaji();
    }
    
    public static void kerja(Karyawan k) {
        if (k instanceof Manajer) {
            Manajer m = (Manajer) k;
            m.rekapLaporan();
        } else if (k instanceof Engineer) {
            Engineer e = (Engineer) k;
            e.periksaMesin();
        }
    }

//    public void kerjaManajer(Manajer m) {
//        m.rekapLaporan();
//    }
//
//    public void kerjaEngineer(Engineer e) {
//        e.periksaMesin();
//    }
}
