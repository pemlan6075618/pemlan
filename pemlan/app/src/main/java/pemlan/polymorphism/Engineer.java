package pemlan.polymorphism;

public class Engineer extends Karyawan {

    public void periksaMesin() {
        System.out.println("Engineer melakukan pemeriksaaan mesin");
    }

    public void hitungGaji() {
        System.out.println("Engineer gajinya Rp. 150.000.000");
    }

}
