package pemlan.encapsulation;

import pemlan.encapsulation2.*;

public class MainClass {

    public static void main(String[] a) {
        Mobil mobil1 = new Mobil();
        mobil1.warna = "BIRU";
        mobil1.pabrikan = "HONDA";
        mobil1.jenis = "SUV";
        mobil1.setJmlRoda(2);
        System.out.println("mobil1 jumlah roda " + mobil1.getJmlRoda());
        Mobil mobil2 = new Mobil();
        mobil2.setJmlRoda(6);
        System.out.println("mobil2 jumlah roda " + mobil2.getJmlRoda());
    }
}
