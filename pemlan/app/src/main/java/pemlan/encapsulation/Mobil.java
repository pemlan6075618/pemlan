package pemlan.encapsulation;

public class Mobil {

    String warna;
    public String pabrikan;
    protected String jenis;
    private Integer jmlRoda;

    public Integer getJmlRoda() {
        return jmlRoda;
    }

    public void setJmlRoda(Integer jmlRoda) {
        if (jmlRoda < 6) {
            this.jmlRoda = 6;
        } else {
            this.jmlRoda = jmlRoda;
        }
    }
}