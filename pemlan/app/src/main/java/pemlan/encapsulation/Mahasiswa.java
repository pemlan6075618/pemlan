package pemlan.encapsulation;

public class Mahasiswa {

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if (!email.contains("@")) {
            System.out.println("email harus mengandung karakter @");
            System.exit(1);
        } else {
            this.email = email;
        }
    }

}
