package pemlan.case3;

import java.util.ArrayList;
import java.util.Scanner;

public class MainClass {

    public static void main(String[] a) {
        ArrayList mahasiswas = new ArrayList();

        Scanner scanner = new Scanner(System.in);
        boolean next = true;
        while (next) {
            System.out.print("masukkan nim : ");
            String nim = scanner.nextLine();

            System.out.print("masukkan nama : ");
            String nama = scanner.nextLine();

            System.out.print("masukkan alamat: ");
            String alamat = scanner.nextLine();

            Mahasiswa mhs = new Mahasiswa();
            mhs.setNim(nim);
            mhs.setNama(nama);
            mhs.setAlamat(alamat);

            System.out.print("masukkan warna mobil : ");
            String warna = scanner.nextLine();

            System.out.print("masukkan jenis mobil : ");
            String jenis = scanner.nextLine();

            System.out.print("masukkan pabrikan mobil : ");
            String pabrikan = scanner.nextLine();

            Mobil mobil = new Mobil();
            mobil.setWarna(warna);
            mobil.setJenis(jenis);
            mobil.setPabrikan(pabrikan);
            mhs.setMobil(mobil);

            mahasiswas.add(mhs);
            System.out.print("tambah lagi? ");
            String tambah = scanner.nextLine();
            if (tambah.equals("t")) {
                next = false;
            }
        }

        System.out.println(
                "==================================");
        for (int i = 0;
                i < mahasiswas.size();
                i++) {
            Mahasiswa mhs = (Mahasiswa) mahasiswas.get(i);
            System.out.println(mhs.getNim() 
                    + " | " + mhs.getNama()
                    + " | " + mhs.getAlamat()
                    + " | " + mhs.getMobil().getWarna()
                    + " | " + mhs.getMobil().getJenis()
                    + " | " + mhs.getMobil().getPabrikan()
            );
        }
    }
}
